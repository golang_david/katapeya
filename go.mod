module gitlab.com/golang_david/katapeya

go 1.15

require (
	github.com/gin-gonic/gin v1.7.4 // indirect
	github.com/go-playground/assert/v2 v2.0.1
	github.com/golang/mock v1.6.0 // indirect
	github.com/stretchr/testify v1.7.0
)
