package personajes

import "errors"

type EstadoPersonaje int

const (
	maxSalud float32 = 1000

	Muerto EstadoPersonaje = iota
	Vivo
)

type Personaje interface {
	HacerDaño(blanco PersonajeConcreto) error
	CurarPersonaje(blanco PersonajeConcreto) error
	GetSalud() int
	GetNivel() int
	GetEstado() EstadoPersonaje
}

func NewPersonaje() *PersonajeConcreto {
	return &PersonajeConcreto{
		salud:    maxSalud,
		nivel:    1,
		estado:   Vivo,
		daño:     500,
		curacion: 250,
	}
}

type PersonajeConcreto struct {
	salud    float32
	nivel    int
	estado   EstadoPersonaje
	daño     float32
	curacion float32
}

func (p *PersonajeConcreto) GetSalud() float32 {
	return p.salud
}

func (p *PersonajeConcreto) GetNivel() int {
	return p.nivel
}

func (p *PersonajeConcreto) GetEstado() EstadoPersonaje {
	return p.estado
}

func (p *PersonajeConcreto) recibirDaño(daño float32) {
	nuevaSalud := p.salud - daño
	if nuevaSalud <= 0 {
		p.salud = 0
		p.estado = Muerto
	} else {
		p.salud = nuevaSalud
	}
}

func (p *PersonajeConcreto) HacerDaño(blanco *PersonajeConcreto) error {
	if p == blanco {
		return errors.New("no puede hacerse daño a si mismo")
	}
	var factor float32 = 1
	diff := p.nivel - blanco.nivel
	if diff >= 5 {
		factor = 1.5
	}
	if diff <= -5 {
		factor = 0.5
	}
	daño := p.daño * factor
	blanco.recibirDaño(daño)
	return nil
}

func (p *PersonajeConcreto) recibirSalud(salud float32) error {
	if p.estado == Muerto {
		return errors.New("el personaje esta muerto no puede curarse")
	}
	nuevaSalud := p.salud + salud
	if nuevaSalud > maxSalud {
		p.salud = maxSalud
	} else {
		p.salud = nuevaSalud
	}
	return nil
}

func (p *PersonajeConcreto) CurarPersonaje(blanco *PersonajeConcreto) error {
	if p != blanco {
		return errors.New("un personaje solo puede curarse a si mismo")
	}
	return blanco.recibirSalud(p.curacion)
}
