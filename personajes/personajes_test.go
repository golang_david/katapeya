package personajes

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewPersonaje(t *testing.T) {
	p := NewPersonaje()
	t.Run("Debe Crear un personaje con Salud 1000, nivel 1 y estado Vivo", func(t *testing.T) {
		assert.Equal(t, maxSalud, p.GetSalud())
		assert.Equal(t, 1, p.GetNivel())
		assert.Equal(t, Vivo, p.GetEstado())
	})
}

func TestPersonajeConcreto_CurarPersonaje(t *testing.T) {
	curandero := NewPersonaje()
	blanco := &PersonajeConcreto{
		salud:    500,
		nivel:    1,
		estado:   Vivo,
		daño:     500,
		curacion: 250,
	}
	t.Run("El personaje curandero al intentar curar a otro personaje que no es el mismo debe recibir un mensaje de error", func(t *testing.T) {
		err := curandero.CurarPersonaje(blanco)
		var saludExp float32 = 500
		assert.Equal(t, "un personaje solo puede curarse a si mismo", err.Error())
		assert.Equal(t, saludExp, blanco.salud)
		assert.Equal(t, Vivo, blanco.estado)
	})
	curandero = &PersonajeConcreto{
		salud:    500,
		nivel:    1,
		estado:   Vivo,
		daño:     500,
		curacion: 250,
	}
	t.Run("El personaje curandero tiene 500 de vida y debe recibir 250 de salud para quedar en 750 y vivo", func(t *testing.T) {
		err := curandero.CurarPersonaje(curandero)
		var saludExp float32 = 750
		assert.Equal(t, nil, err)
		assert.Equal(t, saludExp, curandero.salud)
		assert.Equal(t, Vivo, curandero.estado)
	})
	curandero = &PersonajeConcreto{
		salud:    999,
		nivel:    1,
		estado:   Vivo,
		daño:     500,
		curacion: 250,
	}
	t.Run("El personaje curandero tiene 999 de vida y debe recibir 250 de salud para quedar en 1000 y vivo", func(t *testing.T) {
		err := curandero.CurarPersonaje(curandero)
		assert.Equal(t, nil, err)
		assert.Equal(t, maxSalud, curandero.salud)
		assert.Equal(t, Vivo, curandero.estado)
	})
	curandero = &PersonajeConcreto{
		salud:    0,
		nivel:    1,
		estado:   Muerto,
		daño:     500,
		curacion: 250,
	}
	t.Run("El personaje curandero tiene 0 de salud y esta muerto, se debe recibir un mensaje indicando que no se lo puede curar", func(t *testing.T) {
		err := curandero.CurarPersonaje(curandero)
		var saludExp float32 = 0
		assert.Equal(t, "el personaje esta muerto no puede curarse", err.Error())
		assert.Equal(t, saludExp, curandero.salud)
		assert.Equal(t, Muerto, curandero.estado)
	})
}

func TestPersonajeConcreto_HacerDaño(t *testing.T) {
	atacante := NewPersonaje()
	blanco := NewPersonaje()
	err := atacante.HacerDaño(blanco)
	t.Run("El personaje blanco debe recibir 500 de daño y su salud quedar en 500 pero sigue vivo", func(t *testing.T) {
		var saludExp float32 = 500
		var dañoExp float32 = 500
		assert.Equal(t, nil, err)
		assert.Equal(t, dañoExp, atacante.daño)
		assert.Equal(t, saludExp, blanco.salud)
		assert.Equal(t, Vivo, blanco.estado)
	})
	err = atacante.HacerDaño(blanco)
	t.Run("El personaje blanco debe recibir 500 de daño y su salud quedar en 0 y morir", func(t *testing.T) {
		var saludExp float32 = 500
		var dañoExp float32 = 0
		assert.Equal(t, nil, err)
		assert.Equal(t, saludExp, atacante.daño)
		assert.Equal(t, dañoExp, blanco.salud)
		assert.Equal(t, Muerto, blanco.estado)
	})
	err = atacante.HacerDaño(atacante)
	t.Run("El personaje blanco es igual al atacante, debemos recibir un mensaje que indica que no puede atacarse a si mismo y su salud no debe restarse", func(t *testing.T) {
		var saludExp float32 = 1000
		assert.Equal(t, "no puede hacerse daño a si mismo", err.Error())
		assert.Equal(t, saludExp, atacante.salud)
	})
}

func TestPersonajeConcreto_recibirDaño(t *testing.T) {

}

func TestPersonajeConcreto_recibirSalud(t *testing.T) {

}
